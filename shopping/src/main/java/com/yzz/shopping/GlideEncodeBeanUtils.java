package com.yzz.shopping;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.yzz.shopping.utils.EncryptUtils;
import com.yzz.shopping.utils.LoggerUtils;
import com.yzz.shopping.utils.ShoppingAppInitUtils;
import com.yzz.shopping.utils.ZipHelper;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/22 9:52
 * @Description : EncodeBeanUtils
 */
public class GlideEncodeBeanUtils {
    public static <T> ArrayList<T> encodeCityBeanListNew(String data, Class<T> classOfT, boolean isDecompress) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }

        ArrayList<T> list = new ArrayList<T>();
        try {

            byte[] decryptBase64AES = EncryptUtils.decryptBase64AES(
                    data.getBytes(),
                    ShoppingAppInitUtils.Companion.getInstance().getAES_PARAM_KEY().getBytes(),
                    "AES/CBC/PKCS5Padding",
                    ShoppingAppInitUtils.Companion.getInstance().getAES_PARAM_IV().getBytes()
            );
            final String resultData;
            if (isDecompress) {
                resultData = ZipHelper.decompressToStringForZlib(decryptBase64AES);
            } else {
                resultData = new String(decryptBase64AES);
            }
            LoggerUtils.e("2104", "encodeData: " + resultData);
            if (!TextUtils.isEmpty(resultData)) {
                Gson gson = new Gson();
                JsonArray arry = JsonParser.parseString(resultData).getAsJsonArray();
                for (JsonElement jsonElement : arry) {
                    list.add(gson.fromJson(jsonElement, classOfT));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public static <T> T encodeCityBean(String data, Class<T> classOfT) {
        if (TextUtils.isEmpty(data)) {
            return null;
        }
        T bean = null;
        try {
            byte[] decryptBase64AES = EncryptUtils.decryptBase64AES(
                    data.getBytes(),
                    ShoppingAppInitUtils.Companion.getInstance().getAES_PARAM_KEY().getBytes(),
                    "AES/CBC/PKCS5Padding",
                    ShoppingAppInitUtils.Companion.getInstance().getAES_PARAM_IV().getBytes()
            );
            String resultData = ZipHelper.decompressToStringForZlib(decryptBase64AES);
            LoggerUtils.e("2104", "encodeData: " + resultData);
            if (!TextUtils.isEmpty(resultData)) {
                bean = new Gson().fromJson(resultData, classOfT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }

    public static HashMap<String, Object> getParam(HashMap<String, Object> newParam) {
        //不加密
        final HashMap<String, Object> paramsSign = new HashMap<>();

        if (ShoppingAppInitUtils.Companion.getInstance().getMApplication() != null) {
            paramsSign.put("co", getVersionCode(ShoppingAppInitUtils.Companion.getInstance().getMApplication()));
        }
        paramsSign.put("os", 1);
        if (ShoppingAppInitUtils.Companion.getInstance().getMApplication() != null) {
            paramsSign.put("pkg", ShoppingAppInitUtils.Companion.getInstance().getMApplication().getPackageName());
        }
        //加密
        HashMap<String, Object> passwordMap = new HashMap<>();
        for (String s : newParam.keySet()) {
            passwordMap.put(s, newParam.get(s));
        }
        final JSONObject jsonObject = new JSONObject(passwordMap);
        String data = jsonObject.toString();
        String encrypData = new String(EncryptUtils.encryptAES2Base64(data.getBytes(),
                ShoppingAppInitUtils.Companion.getInstance().getAES_PARAM_KEY().getBytes(),
                "AES/CBC/PKCS5Padding",
                ShoppingAppInitUtils.Companion.getInstance().getAES_PARAM_IV().getBytes()), StandardCharsets.UTF_8);
        paramsSign.put("data", encrypData);
        return paramsSign;
    }

    private static int getVersionCode(Context context) {
        int versionCode = 0;
        try {
            versionCode = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(),
                            0).versionCode;
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }
        return versionCode;
    }
}
