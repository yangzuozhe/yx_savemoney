package com.yzz.shopping;

import static okhttp3.logging.HttpLoggingInterceptor.Level;
import static okhttp3.logging.HttpLoggingInterceptor.Logger;

import android.os.Environment;

import com.yzz.shopping.utils.LoggerUtils;
import com.yzz.shopping.utils.ShoppingAppInitUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public enum ClientFactory {

    INSTANCE;
    private final String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
    private final String NET_DATA_PATH = SDCARD_PATH + File.separator + "net_cache";
    private volatile OkHttpClient okHttpClient;

    private static final int TIMEOUT_READ = 15;
    private static final int TIMEOUT_CONNECTION = 15;
    private final OkHttpClient.Builder mBuilder;

    ClientFactory() {
        mBuilder = new OkHttpClient.Builder();
        if (ShoppingAppInitUtils.Companion.getInstance().getMIsDebug()) {
            mBuilder.addInterceptor(ClientHelper.getHttpLoggingInterceptor());
        }
        Cache cache = new Cache(new File(NET_DATA_PATH), 10 * 1024 * 1024);
        mBuilder.addNetworkInterceptor(ClientHelper.getAutoCacheInterceptor());
        mBuilder.addInterceptor(ClientHelper.getAutoCacheInterceptor());
//        mBuilder.cache(cache);
        mBuilder.retryOnConnectionFailure(true)
                .readTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT_CONNECTION, TimeUnit.SECONDS)
                .build();
    }


    private void onHttpsNoCertficates() {
        try {
            mBuilder.sslSocketFactory(ClientHelper.getSSLSocketFactory()).hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


//    private void onHttpCertficates(int[] certficates, String[] hosts) {
//        mBuilder.socketFactory(ClientHelper.getSSLSocketFactory(ShoppingFragment.Companion.getMApplication(), certficates));
//        mBuilder.hostnameVerifier(ClientHelper.getHostnameVerifier(hosts));
//    }

    //不打印数据
    public OkHttpClient getHttpClient() {
        okHttpClient = mBuilder.build();
        return okHttpClient;
    }

    //打印数据
    public OkHttpClient getOkHttpClient() {
        //日志显示级别
        Level level = Level.BODY;
        //新建log拦截器
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new Logger() {
            @Override
            public void log(String message) {
                LoggerUtils.d("shoppingzcb", "OkHttp====Message:" + message);
            }
        });
        loggingInterceptor.setLevel(level);
        //OkHttp进行添加拦截器loggingInterceptor
        mBuilder.addInterceptor(loggingInterceptor);
        okHttpClient = mBuilder.build();
        return okHttpClient;
    }
}