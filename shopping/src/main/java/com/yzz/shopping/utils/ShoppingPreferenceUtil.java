package com.yzz.shopping.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class ShoppingPreferenceUtil {

    public static final String SHOP_TITLE = "SHOP_TITLE";
    public static final String WHITE_LIST = "ADMIN_WHITE_LIST";

    public static void setTitleParam(Context context, String adValue) {
        if (!TextUtils.isEmpty(adValue)) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString(SHOP_TITLE, adValue);
            edit.apply();
        }
    }

    public static String getTitleParam(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(SHOP_TITLE, null);
    }

    public static void setWhiteListParam(Context context, String adValue) {
        if (!TextUtils.isEmpty(adValue)) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString(WHITE_LIST, adValue);
            edit.apply();
        }
    }

    public static String getWhiteListParam(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(WHITE_LIST, null);
    }
}
