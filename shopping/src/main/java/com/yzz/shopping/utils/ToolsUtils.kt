package com.yzz.shopping.utils

import android.util.TypedValue

/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/6 9:38
 *
 * @Description : ToolsUtils
 */
class ToolsUtils {
    companion object{
         fun dpToPx(dp: Int): Int {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                ShoppingAppInitUtils.instance.mApplication?.resources?.displayMetrics
            ).toInt()
        }
    }
}