package com.yzz.shopping.utils;

import android.content.Context;

import com.kepler.jd.Listener.OpenAppAction;
import com.kepler.jd.login.KeplerApiManager;
import com.kepler.jd.sdk.bean.KelperTask;
import com.kepler.jd.sdk.bean.KeplerAttachParameter;

import org.json.JSONException;


/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/28 13:58
 * @Description : JumpToOtherApp
 */
public class JumpToJingDongAppUtils {
    /**
     * 0 本地拼接， 1 请求京东服务端拼接
     */
    private static final int mRequestLocation = 0;

    private final KeplerAttachParameter mKeplerAttachParameter = new KeplerAttachParameter();//这个是即时性参数  可以设置
    private OpenAppAction mOpenAppAction;
    private KelperTask mKelperTask;


    private JumpToJingDongAppUtils() {
    }

    private static class JumpToJingDongAppUtilsInstance {
        private static final JumpToJingDongAppUtils INSTANCE = new JumpToJingDongAppUtils();
    }

    public static JumpToJingDongAppUtils getInstacne() {
        return JumpToJingDongAppUtilsInstance.INSTANCE;
    }

    /**
     * 初始化京东回调方法
     *
     * @param listener
     */
    private void initJingDongOpenAppAction(IAppListener listener) {
        if (mOpenAppAction == null) {
            mOpenAppAction = status -> {
                {
                    LoggerUtils.d("yyzzjdOpenAppAction", "status=" + status);
                    if (status == OpenAppAction.OpenAppAction_start) {//开始状态未必一定执行，
                        listener.onSuccess();
                        if (mKelperTask != null) {//取消
                            mKelperTask.setCancel(true);
                        }
//                    dialogShow();
                    } else {

                        mKelperTask = null;
//                    dialogDiss();
                    }
                    if (status == OpenAppAction.OpenAppAction_result_NoJDAPP) {
//                        ToastUtils.showShort("您未安装京东app");
                        listener.onFait();
                    } else if (status == OpenAppAction.OpenAppAction_result_BlackUrl) {
                        listener.onFait();
//                    ToastUtils.showShort("url不在白名单，你可以手动打开以下链接地址：" + url + " ,code=" + status);
                    }else if (status == OpenAppAction.OpenAppAction_result_H5){
                        listener.onFait();
                    }else if (status == OpenAppAction.OpenAppAction_result_NoJXApp){
                        listener.onFait();
                    }
                }
            };
        }
    }

    /**
     * 跳转到京东
     *
     * @param context
     * @param urlLocal
     * @param listener
     */
    public void jumpToJingDong(Context context, String urlLocal, IAppListener listener) {
        try {
            initJingDongOpenAppAction(listener);
            mKelperTask = KeplerApiManager.getWebViewService().openJDUrlPage(
                    urlLocal,
                    mKeplerAttachParameter,
                    context,
                    mOpenAppAction,
                    mRequestLocation);
        } catch (JSONException e) {
            e.printStackTrace();
            listener.onFait();
        }
    }

    public interface IAppListener {
        void onSuccess();

        void onFait();
    }


}
