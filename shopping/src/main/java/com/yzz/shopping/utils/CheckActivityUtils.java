package com.yzz.shopping.utils;

import android.app.Activity;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/5/30 10:13
 * @Description : CheckActivityUtils
 */
public class CheckActivityUtils {
    /**
     * 判断Activity是否Destroy
     * @return
     */
    public static boolean isDestroy(Activity mActivity) {
        if (mActivity == null || mActivity.isFinishing() || mActivity.isDestroyed()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNotDestroy(Activity mActivity){
        if (mActivity == null || mActivity.isFinishing() || mActivity.isDestroyed()) {
            return false;
        } else {
            return true;
        }
    }
}
