package com.yzz.shopping.utils

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.yzz.shopping.BuildConfig
import com.yzz.shopping.ui.fragment.ShoppingFragment

fun getAct(fra: Fragment, cxtOk: (Activity) -> Unit) {
    if (fra.isAdded) {
        val act = fra.activity
        if (act != null) {
            if (!act.isFinishing && !act.isDestroyed) {
                cxtOk.invoke(act)
            }
        }
    }
}

fun getCxt(act: Activity?, cxtOk: (Context) -> Unit) {
    if (act?.isFinishing == false && !act.isDestroyed) {
        cxtOk.invoke(act)
    }
}

fun getCxt(fra: Fragment, cxtOk: (Context) -> Unit) {
    if (fra.isAdded) {
        if (fra.context != null) {
            cxtOk.invoke(fra.requireContext())
        }
    }
}

fun logShow(tag: String = "77777", info: String) {
    if (ShoppingAppInitUtils.instance.mIsDebug) {
        LoggerUtils.e(tag, info)
    }
}

fun getToastTest(fra: Fragment, msg: String?) {
    if (ShoppingAppInitUtils.instance.mIsDebug) {
        getCxt(fra) {
            Toast.makeText(it, msg ?: "", Toast.LENGTH_LONG).show()
        }
    }
}

//友盟上报
fun startCallUm(context: Context, key: String, value: String?) {
    try {
        //ShoppingFragment.uMCallBack?.invoke(context, key, "省钱: $value")
    } catch (e: Exception) {
        e.printStackTrace()
    }
}