package com.yzz.shopping.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import com.yzz.shopping.ui.activity.ShoppingWebviewActivity
import com.yzz.shopping.viewmodel.GlideViewModel

/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/4 16:35
 *
 * @Description : JumpToOtherApp
 */
class JumpToOtherApp {

    companion object {

        /**
         * 启动淘宝页面
         */
        fun launchAppActivity(context: Context, ec: String, url: String) {
            try {
                when (ec) {
                    GlideViewModel.IEcType.TAO_BAO.toString() -> {
                        val uri = Uri.parse(url)
                        val intent = Intent(Intent.ACTION_VIEW, uri)
                        intent.setClassName("com.taobao.taobao", "com.taobao.browser.BrowserActivity")
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                    }
                    GlideViewModel.IEcType.JING_DONG.toString() -> {
                        JumpToJingDongAppUtils.getInstacne().jumpToJingDong(context, url,
                            object : JumpToJingDongAppUtils.IAppListener {
                                override fun onSuccess() {
                                }

                                override fun onFait() {
                                    launchWebActivity(context, url)
                                }
                            })
                    }
                    GlideViewModel.IEcType.PIN_DUODUO.toString() -> {
                        //如果安装了应用
                        if (checkAppInstalled(context, "com.xunmeng.pinduoduo")) {
                            //去app
                            val gotoApp = url.replace(
                                "https://mobile.yangkeduo.com/",
                                "pinduoduo://com.xunmeng.pinduoduo/"
                            )
                            val uri = Uri.parse(gotoApp)
                            val intent = Intent(Intent.ACTION_VIEW, uri)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            context.startActivity(intent)
                        } else {
                            launchWebActivity(context, url)
                        }

                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
                //启动失败跳转到浏览器
                launchWebActivity(context, url)

            }
        }

        /**
         * 检测该包名所对应的应用是否存在
         */
        private fun checkAppInstalled(context: Context, pkgName: String): Boolean {
            if (pkgName.isEmpty()) {
                return false
            }
            var packageInfo: PackageInfo? = null
            try {
                packageInfo = context?.packageManager?.getPackageInfo(pkgName, 0);
            } catch (e: PackageManager.NameNotFoundException) {
                packageInfo = null;
                e.printStackTrace();
            }
            return packageInfo != null
        }


        private fun launchWebActivity(context: Context, url: String) {
            val intent = Intent(context, ShoppingWebviewActivity::class.java)
            intent.putExtra(ShoppingWebviewActivity.URL, url)
            context.startActivity(intent)
        }


    }
}