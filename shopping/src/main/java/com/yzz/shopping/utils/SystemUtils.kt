package com.yzz.shopping.utils

import android.app.Activity
import android.content.*
import android.text.TextUtils
import androidx.core.content.ContextCompat

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/24 15:32
 *
 * @Description : SystemUtils
 */

/**
 * 复制到剪贴板
 * @param context 上下文
 * @param string 要复制到内容
 */
fun copyTextToBoard(context: Context, string: String, lable: String? = null) {
    if (TextUtils.isEmpty(string)) {
        return
    }
    val clip =
        ContextCompat.getSystemService(context.applicationContext, ClipboardManager::class.java)
    @Suppress("UsePropertyAccessSyntax")
    clip?.setPrimaryClip(ClipData.newPlainText(lable, string))
}

/**
 * 调用系统安装了的应用分享
 *
 * @param context 上下文
 * @param title 标题
 * @param url url
 */
fun showSystemShareOption(context: Context, title: String, url: String) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, title)
        intent.putExtra(Intent.EXTRA_TEXT, "$title $url")
        if (context !is Activity) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        context.startActivity(Intent.createChooser(intent, null))
    } catch (e: ActivityNotFoundException) {
        e.printStackTrace()
    }
}