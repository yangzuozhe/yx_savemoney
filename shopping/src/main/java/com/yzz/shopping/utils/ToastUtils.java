package com.yzz.shopping.utils;

import android.content.Context;
import android.widget.Toast;

import com.yzz.shopping.ui.fragment.ShoppingFragment;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/24 15:06
 * @Description : ToastUtils
 */
public class ToastUtils {
    public static void showShort(String content) {
        if (ShoppingAppInitUtils.Companion.getInstance().getMApplication() != null) {
            Toast.makeText(ShoppingAppInitUtils.Companion.getInstance().getMApplication(), content, Toast.LENGTH_SHORT).show();
        }
    }

    public static void showLong(Context context, String content) {
        Toast.makeText(context, content, Toast.LENGTH_LONG).show();
    }
}
