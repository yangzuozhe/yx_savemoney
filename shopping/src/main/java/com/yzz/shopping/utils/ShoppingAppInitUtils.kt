package com.yzz.shopping.utils

import android.app.Application
import com.kepler.jd.Listener.AsyncInitListener
import com.kepler.jd.login.KeplerApiManager

/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/31 11:53
 *
 * @Description : ShoppingAppInitUtils
 */
class ShoppingAppInitUtils private constructor() {

    companion object {
        val instance = SingletonHolder.holder
    }

    private object SingletonHolder {
        val holder = ShoppingAppInitUtils()
    }

    var GLIDE_HTTP_URL = ""
    var AES_PARAM_KEY = ""
    var AES_PARAM_IV = ""
    var THEME_COLOR = 0
    var mApplication: Application? = null
    var WEATHER_TYPE = 0
    var mIsDebug = true
    var mUserId : String? = null
    var mUserToken : String? = null

    fun initShopping(
        /**
         * 接口域名
         */
        serviceUrl: String,
        /**
         * application
         */
        application: Application,
        /**
         * 接口 key
         */
        keyStr: String,
        /**
         * 接口 iv
         */
        ivStr: String,
        /**
         * 京东 appkey
         */
        jingDongAppKey: String,
        /**
         * 京东 Secretkey
         */
        jingDongSecretkey: String,
        /**
         * 主题颜色，如 R.color.pink
         */
        themeColor: Int,
        /**
         *  * 0为全部,1为女装
         */
        types: Int,
        /**
         * 是否是 debug 模式
         */
        isDebug: Boolean,
        /**
         * 用户 bean 类
         */
        userId: String?,
        userToken :String?,
    ) {
        GLIDE_HTTP_URL = serviceUrl;
        mApplication = application
        AES_PARAM_KEY = keyStr
        AES_PARAM_IV = ivStr
        THEME_COLOR = themeColor
        WEATHER_TYPE = types
        mIsDebug = isDebug
        mUserId = userId
        mUserToken = userToken
        initJingdong(application, jingDongAppKey, jingDongSecretkey)
    }

    private fun initJingdong(
        application: Application?,
        jingDongAppKey: String,
        jingDongSecretkey: String
    ) {
        if (application != null) {
            // // 注册 这里需要校验 您的证书(shh)，还有包名（package name）,请保证lib工程safe图片是您的
            KeplerApiManager.asyncInitSdk(application,
                jingDongAppKey,
                jingDongSecretkey,
                object : AsyncInitListener {
                    override fun onSuccess() {
                        LoggerUtils.d("yyzzjdinit","onSuccess")
                    }

                    override fun onFailure() {
                        LoggerUtils.d("yyzzjdinit","onFailure")
                    }
                })
        }
    }
}