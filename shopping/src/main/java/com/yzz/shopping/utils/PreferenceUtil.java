/*
 * Copyright © 1 Technology Co., Ltd. All rights reserved.
 * 版权：1（C）2020
 * 作者：Administrator
 * 创建日期：2020年4月6日
 */

package com.yzz.shopping.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Created by T on 2018/6/28
 * Preferenced工具类
 */

public class PreferenceUtil {


    public static void put(String key, String value) {
        putString(ShoppingAppInitUtils.Companion.getInstance().getMApplication(), key, value);
    }



    private static void putString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(ShoppingAppInitUtils.Companion.getInstance().getMApplication())
                .getString(key, defValue);
    }
}
