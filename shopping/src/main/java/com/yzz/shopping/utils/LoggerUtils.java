package com.yzz.shopping.utils;

import android.text.TextUtils;
import android.util.Log;


/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/1 17:29
 * @Description : LoggerUtils
 */
public class LoggerUtils {

    public static void d(String tag, String msg) {
        if (ShoppingAppInitUtils.Companion.getInstance().getMIsDebug()) {
            if (checkText(tag, msg)) {
                Log.d(tag, msg);
            }
        }
    }

    public static void e(String tag, String msg) {
        if (ShoppingAppInitUtils.Companion.getInstance().getMIsDebug()) {
            if (checkText(tag, msg)) {
                Log.e(tag, msg);
            }
        }
    }

    public static void i(String tag, String msg) {
        if (ShoppingAppInitUtils.Companion.getInstance().getMIsDebug()) {
            if (checkText(tag, msg)) {
                Log.i(tag, msg);
            }
        }
    }

    private static boolean checkText(String tag, String msg) {
        return !TextUtils.isEmpty(tag) && !TextUtils.isEmpty(msg);
    }

}
