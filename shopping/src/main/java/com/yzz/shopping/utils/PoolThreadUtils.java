package com.yzz.shopping.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 可重用固定线程数
 */
public class PoolThreadUtils {

    private ExecutorService mFixedThreadPool;

    private PoolThreadUtils() {

    }

    private static class PoolThreadUtilsInstance {
        private static final PoolThreadUtils INSTANCE = new PoolThreadUtils();
    }

    public static PoolThreadUtils getInstance() {
        return PoolThreadUtilsInstance.INSTANCE;
    }

    public void execute(Runnable runnable) {
        if (mFixedThreadPool == null) {
            mFixedThreadPool = newFixThreadPool();
        }
        mFixedThreadPool.execute(runnable);
    }

    private ExecutorService newFixThreadPool() {

        return new ThreadPoolExecutor(3, 5, 5, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(30));
    }


}
