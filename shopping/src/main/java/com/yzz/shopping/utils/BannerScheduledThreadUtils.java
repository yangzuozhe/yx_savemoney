package com.yzz.shopping.utils;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BannerScheduledThreadUtils {
    private ScheduledExecutorService scheduledThreadPool;
    private boolean mIsTouchBanner = false;

    private BannerScheduledThreadUtils() {

    }

    public void setIsTouchBanner(boolean mIsTouchBanner) {
        this.mIsTouchBanner = mIsTouchBanner;
    }

    private static class ThreadPoolUtilsInstance {
        private static final BannerScheduledThreadUtils INSTANCE = new BannerScheduledThreadUtils();
    }

    public static BannerScheduledThreadUtils getInstance() {
        return ThreadPoolUtilsInstance.INSTANCE;
    }

    public void execute(IScheduledThreadListener listener) {
        if (scheduledThreadPool == null) {
            //创建Scheduled线程池
            scheduledThreadPool = new ScheduledThreadPoolExecutor(1);
            scheduledThreadPool.scheduleWithFixedDelay(() -> {
                if (!mIsTouchBanner) {
                    listener.onExecute();
                }
            }, 2, 2, TimeUnit.SECONDS);
        }

    }

    public void showDown() {
        if (scheduledThreadPool != null) {
            scheduledThreadPool.shutdown();
            scheduledThreadPool = null;
        }
    }

    public interface IScheduledThreadListener {
        void onExecute();
    }
}
