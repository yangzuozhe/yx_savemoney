package com.yzz.shopping.utils

import android.content.Context
import android.net.ConnectivityManager
import com.yzz.shopping.ui.fragment.ShoppingFragment


/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/7 15:55
 *
 * @Description : NetWorkUtils
 */
class NetWorkUtils {
    companion object {
        fun hasInternet(): Boolean {
            return (ShoppingAppInitUtils.instance.mApplication?.getSystemService(
                Context.CONNECTIVITY_SERVICE
            ) as ConnectivityManager).activeNetworkInfo != null
        }
    }
}