package com.yzz.shopping.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yzz.shopping.utils.ShoppingSearchHistoryUtils
import kotlinx.coroutines.launch

/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/3 10:37
 *
 * @Description : SearchViewModel
 */
class SearchViewModel : ViewModel() {
    val searchListLiveData by lazy {
        MutableLiveData<MutableList<String?>?>()
    }

    val mIsHistoryVisibility = MutableLiveData(true)
    companion object{
        var SEARCH_CONTENT = ""
    }

    val mSearchContentLiveData by lazy {
        MutableLiveData<String?>(null)
    }


    fun getSearchHistoryList() {
        val list = ShoppingSearchHistoryUtils.getInfoList()
        searchListLiveData.value = list
    }


    fun addNewHistoryInfo(string: String) {
        searchListLiveData.value = ShoppingSearchHistoryUtils.addNewInfo(string)
    }

    fun clearHistoryInfo() {
        ShoppingSearchHistoryUtils.clearInfo()
        searchListLiveData.value = null
    }

    /**
     * 搜索内容
     */
    fun searchContent(content: String) {
        viewModelScope.launch {
            SEARCH_CONTENT = content
            mSearchContentLiveData.postValue(content)
        }
    }
}