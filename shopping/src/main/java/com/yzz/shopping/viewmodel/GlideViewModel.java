package com.yzz.shopping.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.yzz.shopping.GlideHttpUtils;
import com.yzz.shopping.entity.GlideRecommendBean;
import com.yzz.shopping.utils.LoggerUtils;
import com.yzz.shopping.utils.PoolThreadUtils;

import java.util.ArrayList;

import kotlin.Pair;


/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/22 10:20
 * @Description : GlideViewModel
 */
public class GlideViewModel extends AndroidViewModel {
    private static final int PAGE_SIZE = 20;
    private int mPageIndex = 1;
    private int mHomePageIndex = 1;
    private int mSearchPageIndex = 1;
    private Application mApplication;

    public GlideViewModel(@NonNull Application application) {
        super(application);
        mApplication = application;
    }

    public @interface IEcName {
        String TAO_BAO_NAME = "淘宝";
        String JING_DONG_NAME = "京东";
        String PIN_DUODUO_NAME = "拼多多";
        String TIAN_MAO = "天猫";
        String JU_HE_NAME = "全部";

    }

    public @interface IEcType {
        int TAO_BAO = 1;
        int JING_DONG = 2;
        int PIN_DUODUO = 3;
        int JU_HE = 4;
    }

    public @interface IStoryType {
        /**
         * 综合
         */
        Pair<Integer, String> complex = new Pair<>(0, "综合");

        /**
         * 佣金比例降序
         */
        Pair<Integer, String> Descending = new Pair<>(1, "佣金比例");
        /**
         * 销量降序
         */
        Pair<Integer, String> SalesInDescendingOrder = new Pair<>(2, "销量");
        /**
         * 价格降序
         */
        Pair<Integer, String> PriceDescending = new Pair<>(3, "券后价");
        /**
         * 价格升序
         */
        Pair<Integer, String> PriceAscending = new Pair<>(4, "券后价");
    }

    private MutableLiveData<Pair<Integer, ArrayList<GlideRecommendBean>>> mGlideRecommendBeanLiveData;
    private MutableLiveData<Pair<Integer, ArrayList<GlideRecommendBean>>> mHomeRecommendBeanLiveData;
    private MutableLiveData<Pair<Integer, ArrayList<GlideRecommendBean>>> mSearchBeanLiveData;
    private MutableLiveData<Pair<String, String>> mDataLiveData;

    public LiveData<Pair<Integer, ArrayList<GlideRecommendBean>>> getGlideRecommendBeanLiveData() {
        if (mGlideRecommendBeanLiveData == null) {
            mGlideRecommendBeanLiveData = new MutableLiveData<>();
        }
        return mGlideRecommendBeanLiveData;
    }

    public LiveData<Pair<Integer, ArrayList<GlideRecommendBean>>> getHomeRecommendBeanLiveData() {
        if (mHomeRecommendBeanLiveData == null) {
            mHomeRecommendBeanLiveData = new MutableLiveData<>();
        }
        return mHomeRecommendBeanLiveData;
    }

    public LiveData<Pair<Integer, ArrayList<GlideRecommendBean>>> getSearchBeanLiveData() {
        if (mSearchBeanLiveData == null) {
            mSearchBeanLiveData = new MutableLiveData<>();
        }
        return mSearchBeanLiveData;
    }

    public LiveData<Pair<String, String>> getDataLiveData() {
        if (mDataLiveData == null) {
            mDataLiveData = new MutableLiveData<>();
        }
        return mDataLiveData;
    }

    /**
     * 用于真正的推荐模块
     * @param ec
     * @param isFirst
     */
    public void requestRecommend(int ec, boolean isFirst) {
        if (isFirst) {
            mPageIndex = 1;
        }
        PoolThreadUtils.getInstance().execute(() -> GlideHttpUtils.getInstacne().requestRecommend(ec, PAGE_SIZE, mPageIndex, new GlideHttpUtils.GlideListener<ArrayList<GlideRecommendBean>>() {
            @Override
            public void onSuccess(ArrayList<GlideRecommendBean> bean) {
                final Pair<Integer, ArrayList<GlideRecommendBean>> pair = new Pair<>(mPageIndex, bean);
                mGlideRecommendBeanLiveData.postValue(pair);
                mPageIndex++;
            }

            @Override
            public void onError(String msg) {
                mGlideRecommendBeanLiveData.postValue(null);
            }
        }));

    }

    /**
     * 用于主页的推荐数据
     * @param ec
     * @param isFirst
     */
    public void requestHomeRecommend(int ec, boolean isFirst) {
        if (isFirst) {
            mHomePageIndex = 1;
        }
        PoolThreadUtils.getInstance().execute(() -> GlideHttpUtils.getInstacne().requestHomeRecommend(ec, PAGE_SIZE, mHomePageIndex, new GlideHttpUtils.GlideListener<ArrayList<GlideRecommendBean>>() {
            @Override
            public void onSuccess(ArrayList<GlideRecommendBean> bean) {
                final Pair<Integer, ArrayList<GlideRecommendBean>> pair = new Pair<>(mHomePageIndex, bean);
                mHomeRecommendBeanLiveData.postValue(pair);
                mHomePageIndex++;
            }

            @Override
            public void onError(String msg) {
                mHomeRecommendBeanLiveData.postValue(null);
            }
        }));

    }

    public void requestSearch(int ec, String keyword, boolean isCoupon, int sortName, boolean isFirst) {
        LoggerUtils.d("yyzzkeyword", "keyword" + keyword);
        if (isFirst) {
            mSearchPageIndex = 1;
        }
        GlideHttpUtils.getInstacne().requestSearch(ec, keyword, PAGE_SIZE, mSearchPageIndex, isCoupon(isCoupon), sortName, new GlideHttpUtils.GlideListener<ArrayList<GlideRecommendBean>>() {
            @Override
            public void onSuccess(ArrayList<GlideRecommendBean> bean) {
//                LoggerUtils.d("yyzz2", bean.toString());
                final Pair<Integer, ArrayList<GlideRecommendBean>> pair = new Pair<>(mSearchPageIndex, bean);
                mSearchBeanLiveData.postValue(pair);
                mSearchPageIndex++;
            }

            @Override
            public void onError(String e) {
//                LoggerUtils.d("yyzz2", e);
                final Pair<Integer, ArrayList<GlideRecommendBean>> pair = new Pair<>(mSearchPageIndex, null);
                mSearchBeanLiveData.postValue(pair);
            }
        });
    }

    /**
     * @param isCoupon
     * @return 有无优惠券，0表示全部，1表示只看优惠券商品
     */
    private int isCoupon(boolean isCoupon) {
        if (isCoupon) {
            return 1;
        } else {
            return 0;
        }
    }

    public void jumpUrl(String materialUrl, String couponUrl, String ec) {
        GlideHttpUtils.getInstacne().requestJumUrl(materialUrl, couponUrl, ec, new GlideHttpUtils.GlideListener<String>() {
            @Override
            public void onSuccess(String bean) {
                final Pair<String, String> pair = new Pair<>(ec, bean);
//                LoggerUtils.d("yyzz3",bean.toString());
                mDataLiveData.postValue(pair);
            }

            @Override
            public void onError(String e) {
//                LoggerUtils.d("yyzz3",e);
                mDataLiveData.postValue(null);
            }
        });
    }
}
