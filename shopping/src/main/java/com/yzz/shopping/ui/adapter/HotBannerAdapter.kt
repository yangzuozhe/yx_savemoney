package com.yzz.shopping.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.style.ImageSpan
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.youth.banner.adapter.BannerAdapter
import com.yzz.shopping.GlideHttpUtils
import com.yzz.shopping.R
import com.yzz.shopping.entity.GlideRecommendBean
import com.yzz.shopping.inter.IItemSelectListener
import com.yzz.shopping.ui.viewholder.ShoppingViewHolder
import com.yzz.shopping.utils.*
import com.yzz.shopping.viewmodel.GlideViewModel

/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/6 17:02
 *
 * @Description : HotBannerAdapter
 */
class HotBannerAdapter(
    private val mFragmentActivity: FragmentActivity?,
    datas: MutableList<GlideRecommendBean?>?
) :
    BannerAdapter<GlideRecommendBean?, ShoppingViewHolder>(datas) {

    private var mItemSelectListener: IItemSelectListener<GlideRecommendBean?>? = null

    @SuppressLint("SetTextI18n")
    private fun getData(holder: ShoppingViewHolder, bean: GlideRecommendBean?) {
        holder.mShoppingImg?.let {
            mFragmentActivity?.let { fragmentActivity ->
                if (!CheckActivityUtils.isDestroy(fragmentActivity)) {
                    Glide.with(fragmentActivity).asBitmap().load(bean?.imageUrl).apply(
                        RequestOptions().transform(RoundedCorners(ToolsUtils.dpToPx(6)))
                            .placeholder(R.mipmap.icon_default)
                            .dontAnimate()
                            .fallback(R.mipmap.icon_default)
                            .error(R.mipmap.icon_default)
                    ).into(it)
                }
            }
        }
        holder.mTvSaveMoney?.text = ""
        holder.mTvPrice?.text = "¥${bean?.lowestCouponPrice}"
        val specialPrice = bean?.discount
        if (TextUtils.equals(specialPrice, "0") || TextUtils.isEmpty(specialPrice)) {
            holder.mIvFlashing?.visibility = View.INVISIBLE
            holder.mTvSaveMoney?.visibility = View.INVISIBLE
        } else {
            holder.mIvFlashing?.visibility = View.VISIBLE
            holder.mTvSaveMoney?.visibility = View.VISIBLE
            holder.mTvSaveMoney?.text = "已补贴${specialPrice}元"
        }

        val spann = SpannableString("  ${bean?.skuName}")
        val imageSpan =
            holder.mIvShoppingType?.context?.let {
                ImageSpan(it, getImgType(bean?.ecName ?: ""))
            }
        spann.setSpan(imageSpan, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        holder.mIvShoppingType?.text = spann
        holder.mTvSellNumber?.text = "已售${bean?.inOrderCount30Days}"
        holder.mTvShoppingShopName?.text = bean?.shopName
        val spann2 = SpannableString("原价¥${bean?.price}")
        val strikethroughSpan = StrikethroughSpan()
        spann2.setSpan(strikethroughSpan, 0, spann2.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        holder.mTvOriginalPrice?.text = spann2
        holder.mShoppingView?.setOnClickListener {
            val newBean = bean
            if (NoFastClickUtils.isFastClick()) {
                return@setOnClickListener
            }
            newBean?.let { it1 -> mItemSelectListener?.onClick(it1) }
            if (TextUtils.equals(newBean?.ec, GlideViewModel.IEcType.TAO_BAO.toString())) {
                it?.context?.let { it1 ->
                    newBean?.let { b ->
                        JumpToOtherApp.launchAppActivity(
                            it1,
                            b.ec,
                            b.couponUrl
                        )
                    }

                }
            } else {
                jumpUrl(it.context, newBean?.materialUrl, newBean?.couponUrl, newBean?.ec)
            }
        }
    }

    fun setItemListener(listener: IItemSelectListener<GlideRecommendBean?>?) {
        mItemSelectListener = listener
    }

    private fun getImgType(ecName: String): Int {
        return when (ecName) {
            GlideViewModel.IEcName.TAO_BAO_NAME -> {
                R.mipmap.icon_taobao
            }
            GlideViewModel.IEcName.JING_DONG_NAME -> {
                R.mipmap.icon_jingdong
            }
            GlideViewModel.IEcName.PIN_DUODUO_NAME -> {
                R.mipmap.icon_pinduoduo
            }
            GlideViewModel.IEcName.TIAN_MAO -> {
                R.mipmap.icon_tianmao
            }
            else -> {
                0
            }
        }
    }

    private fun jumpUrl(context: Context, materialUrl: String?, couponUrl: String?, ec: String?) {
        GlideHttpUtils.getInstacne().requestJumUrl(
            materialUrl,
            couponUrl,
            ec,
            object : GlideHttpUtils.GlideListener<String?> {


                override fun onError(e: String) {
                    ToastUtils.showShort("网络请求错误")
                }

                override fun onSuccess(str: String?) {
                    if (str != null && ec != null) {
                        JumpToOtherApp.launchAppActivity(context, ec, str)
                    } else {
                        ToastUtils.showShort("网络请求错误")
                    }


                }
            })

    }


    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): ShoppingViewHolder {
        val view = LayoutInflater.from(parent?.context)
            .inflate(R.layout.item_banner, parent, false)
        view.layoutParams.apply {
            width = ViewGroup.LayoutParams.MATCH_PARENT
            //viewpager2 宽高得等于 match
            height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        return ShoppingViewHolder(view)
    }

    override fun onBindView(
        holder: ShoppingViewHolder?,
        data: GlideRecommendBean?,
        position: Int,
        size: Int
    ) {
        holder?.let { getData(it, data) }
    }


}