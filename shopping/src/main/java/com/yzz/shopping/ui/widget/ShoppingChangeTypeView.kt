package com.yzz.shopping.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.yzz.shopping.R
import com.yzz.shopping.entity.ShoppingTypeBean
import com.yzz.shopping.utils.ShoppingAppInitUtils
import com.yzz.shopping.viewmodel.GlideViewModel
import kotlinx.android.synthetic.main.view_shopping_change_type.view.*

/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/4 11:11
 *
 * @Description : ShoppingChangeTypeView
 */
class ShoppingChangeTypeView : FrameLayout {
    private var mIsAfterCountUp = false
    private var mIsOnlyShowCoupon = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.view_shopping_change_type, this)
    }

    fun addTabViewList(
        list: MutableList<ShoppingTypeBean?>?,
        changeTypeListener: IChangeTypeListener
    ) {
        list?.forEachIndexed { index, bean ->
            val tab = mShoppingTabType?.newTab()
            tab?.let { t ->
                val view: View = LayoutInflater.from(context)
                    .inflate(R.layout.view_custom_tab_shopping_type, null)
                val textView = view.findViewById<AppCompatTextView>(R.id.mTvShoppingTabName)
                val imageView = view.findViewById<AppCompatImageView>(R.id.mIvShoppingTab)
                textView?.text = bean?.typeName
                if (index == 0) {
                    setTextViewThemeColor(textView)
                }
                if (bean?.typeUpImg != null && bean.typeUpImg != 0) {
                    imageView?.visibility = View.VISIBLE
                    imageView?.setBackgroundResource(bean.typeUnSelectImg)
                    imageView?.rotation = bean.typeImgRotation.toFloat()
                }
                t.customView = view
                t.tag = bean;
                mShoppingTabType?.addTab(t)
            }
        }
        mShoppingFilterType?.let { fView ->
            fView.setOnClickListener {
                mIsOnlyShowCoupon = !mIsOnlyShowCoupon
                setImgViewResource(it.findViewById(R.id.mIvShoppingTab))
                changeTypeListener.onSelect(null, mIsOnlyShowCoupon)
            }
            fView.findViewById<AppCompatTextView>(R.id.mTvShoppingTabName)?.apply {
                text = "优惠券"
                setTextColor(ContextCompat.getColor(context, R.color.black))
            }
            fView.findViewById<AppCompatImageView>(R.id.mIvShoppingTab)?.let {
                it.visibility = VISIBLE
                setImgViewResource(it)
            }
        }

        mShoppingTabType?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val cusView = tab?.customView
                val textView = cusView?.findViewById<AppCompatTextView>(R.id.mTvShoppingTabName)
                val imageView = cusView?.findViewById<AppCompatImageView>(R.id.mIvShoppingTab)
                setTextViewThemeColor(textView)
                val bean: ShoppingTypeBean? = tab?.tag as ShoppingTypeBean?
                if (bean?.typeUpImg != null && bean.typeUpImg != 0) {
                    mIsAfterCountUp = true
                    imageView?.visibility = View.VISIBLE
                    imageView?.setBackgroundResource(bean.typeUpImg)
                    imageView?.rotation = 180f
                }
                bean?.type?.let { changeTypeListener.onSelect(it,mIsOnlyShowCoupon) }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val cusView = tab?.customView
                val textView = cusView?.findViewById<AppCompatTextView>(R.id.mTvShoppingTabName)
                val imageView = cusView?.findViewById<AppCompatImageView>(R.id.mIvShoppingTab)
                textView?.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.black
                    )
                )
                val bean: ShoppingTypeBean? = tab?.tag as ShoppingTypeBean?
                if (bean?.typeUnSelectImg != null && bean.typeUnSelectImg != 0) {
                    imageView?.visibility = View.VISIBLE
                    imageView?.setBackgroundResource(bean.typeUnSelectImg)
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                val cusView = tab?.customView
                val textView = cusView?.findViewById<AppCompatTextView>(R.id.mTvShoppingTabName)
                val imageView = cusView?.findViewById<AppCompatImageView>(R.id.mIvShoppingTab)
                setTextViewThemeColor(textView)
                val bean: ShoppingTypeBean? = tab?.tag as ShoppingTypeBean?
                if (bean?.typeUpImg != null && bean.typeUpImg != 0) {
                    mIsAfterCountUp = !mIsAfterCountUp
                    imageView?.visibility = View.VISIBLE
                    imageView?.setBackgroundResource(bean.typeUpImg)
                    if (mIsAfterCountUp) {
                        imageView?.rotation = 180f
                        changeTypeListener.onSelect(bean.type,mIsOnlyShowCoupon)
                    } else {
                        imageView?.rotation = 0f
                        changeTypeListener.onSelect(GlideViewModel.IStoryType.PriceDescending.first,mIsOnlyShowCoupon)
                    }

                }
            }

        })
    }

    private fun setTextViewThemeColor(textView: AppCompatTextView?) {
        if (ShoppingAppInitUtils.instance.THEME_COLOR != 0) {
            textView?.setTextColor(
                ContextCompat.getColor(
                    context,
                    ShoppingAppInitUtils.instance.THEME_COLOR
                )
            )
        } else {
            textView?.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.pink
                )
            )
        }
    }

    private fun setImgViewResource(imageView: AppCompatImageView?) {
        imageView?.setImageResource(if (mIsOnlyShowCoupon) R.mipmap.icon_coupon_on else R.mipmap.icon_coupon_un)
    }

    fun interface IChangeTypeListener {
        fun onSelect(str: Int?, bo: Boolean)
    }


}