package com.yzz.shopping.ui.activity

import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.yzz.shopping.R
import com.yzz.shopping.entity.MenuBean
import com.yzz.shopping.ui.adapter.ShoppingVpAdapter
import com.yzz.shopping.utils.ShoppingPreferenceUtil
import com.yzz.shopping.utils.ToastUtils
import com.yzz.shopping.viewmodel.GlideViewModel
import com.yzz.shopping.viewmodel.SearchViewModel
import kotlinx.android.synthetic.main.activity_shopping_search.*
import kotlinx.android.synthetic.main.fragment_shpping_search.*
import kotlinx.coroutines.launch

class ShoppingSearchActivity : AppCompatActivity() {
    var mSearchViewModel: SearchViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_search)
        initSearchToolbar()
        initViewModel()
        val menuStr = ShoppingPreferenceUtil.getTitleParam(this)
        val menuList = arrayListOf<MenuBean>()
        if (!TextUtils.isEmpty(menuStr)) {
            val list = menuStr.split(",")
            list.forEach {
                menuList.add(MenuBean(it, getType(it).toString()))
            }
        } else {
            getEmpty(menuList)
        }
        mShoppingViewPager.adapter = ShoppingVpAdapter(this, menuList)
        mShoppingTabLayout?.setListAdapter(menuList, mShoppingViewPager)
        if (menuList.isNotEmpty()) {
            mShoppingViewPager?.offscreenPageLimit = menuList.size
        }
    }

    private fun initViewModel() {
        mSearchViewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        lifecycleScope.launch {
            mSearchViewModel?.mSearchContentLiveData?.observe(this@ShoppingSearchActivity) {
                mShoppingSearchToolbar?.setEditTextContent(it)
            }
        }
    }

    private fun getType(string: String): Int {
        return when (string) {
            GlideViewModel.IEcName.TAO_BAO_NAME -> {
                GlideViewModel.IEcType.TAO_BAO
            }
            GlideViewModel.IEcName.JING_DONG_NAME -> {
                GlideViewModel.IEcType.JING_DONG
            }
            GlideViewModel.IEcName.PIN_DUODUO_NAME -> {
                GlideViewModel.IEcType.PIN_DUODUO
            }
            GlideViewModel.IEcName.JU_HE_NAME -> {
                GlideViewModel.IEcType.JU_HE
            }
            else -> {
                0
            }
        }
    }

    private fun initSearchToolbar() {
        mShoppingSearchToolbar?.setOnLeftClick {
            finish()
        }
        mShoppingSearchToolbar?.setOnRightClick {
            val text = mShoppingSearchToolbar.getEditTextContent()
            searchText(text)
        }
        mShoppingSearchToolbar?.setOnSearchListener {
            val text = it.toString()
            searchText(text)

        }
        mShoppingSearchToolbar?.setEditTextClick {
            mSearchViewModel?.mIsHistoryVisibility?.value = true
        }
        mShoppingSearchToolbar?.setOnClearSearchListener {
            mSearchViewModel?.mIsHistoryVisibility?.value = true
        }
    }

    private fun searchText(text: String?) {
        if (!TextUtils.isEmpty(text)) {
            text?.let {
                mSearchViewModel?.addNewHistoryInfo(it)
                mSearchViewModel?.searchContent(it)
            }
        } else {
            ToastUtils.showShort("不能为空")
        }
    }

    private fun getEmpty(menuList: ArrayList<MenuBean>) {
        val taobao = GlideViewModel.IEcName.TAO_BAO_NAME
        val jingdong = GlideViewModel.IEcName.JING_DONG_NAME
        val pinduoduo = GlideViewModel.IEcName.PIN_DUODUO_NAME
        val juhe = GlideViewModel.IEcName.JU_HE_NAME
        menuList.add(MenuBean(juhe, getType(juhe).toString()))
        menuList.add(MenuBean(taobao, getType(taobao).toString()))
        menuList.add(MenuBean(jingdong, getType(jingdong).toString()))
        menuList.add(MenuBean(pinduoduo, getType(pinduoduo).toString()))
    }

    override fun onDestroy() {
        mShoppingSearchToolbar?.hideSoftInput()
        super.onDestroy()
    }
}