package com.yzz.shopping.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.yzz.shopping.entity.MenuBean
import com.yzz.shopping.ui.fragment.ShoppingSearchFragment

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/24 10:35
 *
 * @Description : ShoppingVpAdapter
 */
class ShoppingVpAdapter(fragmentActivity: FragmentActivity, private val mList: MutableList<MenuBean>) :
    FragmentStateAdapter(fragmentActivity) {


    override fun getItemCount(): Int = mList.size

    override fun createFragment(position: Int): Fragment {
        val menuBean = mList[position]
        return ShoppingSearchFragment.newInstance(menuBean)
    }

}