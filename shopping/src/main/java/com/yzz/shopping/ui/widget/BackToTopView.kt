package com.yzz.shopping.ui.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.yzz.shopping.R
import com.yzz.shopping.utils.ShoppingAppInitUtils
import com.yzz.shopping.utils.ToolsUtils


/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/7 14:21
 *
 * @Description : GotoTopView
 */
class BackToTopView : AppCompatImageView {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        setImageResource(R.mipmap.icon_back_to_top)
        setPadding(
            ToolsUtils.dpToPx(9),
            ToolsUtils.dpToPx(9),
            ToolsUtils.dpToPx(9),
            ToolsUtils.dpToPx(9)
        )
    }


    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        val color = if (ShoppingAppInitUtils.instance.THEME_COLOR != 0) {
            ContextCompat.getColor(context, ShoppingAppInitUtils.instance.THEME_COLOR)
        } else {
            ContextCompat.getColor(context, R.color.pink)
        }
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = color
        paint.style = Paint.Style.FILL
        canvas?.drawCircle(width / 2f, height / 2f, width / 2f, paint)
        super.onDraw(canvas)

    }


}