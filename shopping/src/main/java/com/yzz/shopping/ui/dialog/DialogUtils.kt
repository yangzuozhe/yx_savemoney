package com.yzz.shopping.ui.dialog

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.yzz.shopping.R

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/25 14:00
 *
 * @Description : DialogUtils
 */
class DialogUtils {
    private var mLoadingDialog: LoadingDialog? = null
    private var mAlertDialog : AlertDialog? = null

    companion object {
        val instance = DialogUtilsHolder.holder
    }

    private object DialogUtilsHolder {
        val holder = DialogUtils()
    }

    fun showLoadingDialog(context: Context) {
        closeLoadingDialog()
        mLoadingDialog = LoadingDialog(context)
        mLoadingDialog?.show()
    }

    fun closeLoadingDialog(){
        if (mLoadingDialog != null) {
            mLoadingDialog?.dismiss()
        }
    }

    fun showIsAdminPostGoods(context: Context, listener : IDialogListener){
        mAlertDialog?.let {
            if (it.isShowing){
                it.dismiss()
            }
        }
        mAlertDialog = AlertDialog.Builder(context).setMessage(R.string.is_admin_up_goods)
            .setPositiveButton(R.string.positive) { dialog, witch ->
                listener.onSuccess()
                dialog.dismiss()
            }.setNegativeButton(R.string.cancel) { dialog, witch ->
                dialog.dismiss()
            }.create()
        mAlertDialog?.show()
    }

    fun interface IDialogListener{
        fun onSuccess()
    }


}