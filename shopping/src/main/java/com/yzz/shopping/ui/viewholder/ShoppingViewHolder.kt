package com.yzz.shopping.ui.viewholder

import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.recyclerview.widget.RecyclerView
import com.yzz.shopping.R

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/24 11:15
 *
 * @Description : ShoppingViewHolder
 */
open class ShoppingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val mShoppingImg: ImageView? = itemView.findViewById(R.id.mShoppingImg)
    val mTvPrice: AppCompatTextView? = itemView.findViewById(R.id.mTvPrice)
    val mTvSaveMoney: AppCompatTextView? = itemView.findViewById(R.id.mTvSaveMoney)
    val mIvShoppingType: AppCompatTextView? = itemView.findViewById(R.id.mIvShoppingType)
    val mTvSellNumber: AppCompatTextView? = itemView.findViewById(R.id.mTvSellNumber)
    val mShoppingView: ConstraintLayout? = itemView.findViewById(R.id.mShoppingView)
    //店铺名称
    val mTvShoppingShopName: AppCompatTextView? = itemView.findViewById(R.id.mTvShoppingShopName)
    val mTvOriginalPrice: AppCompatTextView? = itemView.findViewById(R.id.mTvOriginalPrice)
    val mIvFlashing : AppCompatImageView? = itemView.findViewById(R.id.mIvFlashing)
}