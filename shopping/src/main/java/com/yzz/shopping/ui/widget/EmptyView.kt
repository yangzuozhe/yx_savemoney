package com.yzz.shopping.ui.widget

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import com.yzz.shopping.R
import com.yzz.shopping.ui.fragment.ShoppingFragment
import com.yzz.shopping.utils.ShoppingAppInitUtils
import kotlinx.android.synthetic.main.fragment_shpping_search.*
import kotlinx.android.synthetic.main.view_empty.view.*

class EmptyView : FrameLayout {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.view_empty, this)
        if (ShoppingAppInitUtils.instance.THEME_COLOR != 0) {
            mIvRetry?.setImageDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        ShoppingAppInitUtils.instance.THEME_COLOR
                    )
                )
            )
        } else {
            mIvRetry?.setImageDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.pink
                    )
                )
            )
        }
    }

//    fun setEmptyImage(imageResource: Int) {
//        mIvLoadError?.setImageResource(imageResource)
//    }

    private fun setErrorMsg(text: String) {
        mTvErrorMsg?.text = text
    }

    private fun setErrorSubMsg(text: String) {
        mTvErrorSubMsg?.text = text
    }

    fun setRetryCLick(@Nullable l: OnClickListener) {
        mFlRetry?.setOnClickListener(l)
    }

    private fun setRetryVisibility(isShow: Boolean) {
        if (isShow) {
            mTvRetry?.visibility = View.VISIBLE
            mIvRetry?.visibility = View.VISIBLE
        } else {
            mTvRetry?.visibility = View.GONE
            mIvRetry?.visibility = View.GONE
        }
    }

    fun setEmptyMessage() {
        setErrorSubMsg("");
        setErrorMsg("暂时没有内容")
        setEmptyIsShow(true)
        setRetryVisibility(false)
    }

    fun setErrorMessage() {
        context?.getString(R.string.loading_error)?.let { setErrorMsg(it) }
        context?.getString(R.string.try_again)?.let { setErrorSubMsg(it) }
        setEmptyIsShow(true)
        setRetryVisibility(true)
    }

    fun setEmptyIsShow(boolean: Boolean) {
        visibility = if (boolean) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }


}