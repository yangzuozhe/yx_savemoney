package com.yzz.shopping.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.yzz.shopping.R;
import com.yzz.shopping.entity.MenuBean;
import com.yzz.shopping.ui.fragment.ShoppingFragment;
import com.yzz.shopping.utils.ShoppingAppInitUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomTabLayout extends FrameLayout {
    private TabLayout tabLayoutCustom;
    private List<MenuBean> mMenuBeanList = new ArrayList<>();

    public CustomTabLayout(@NonNull Context context) {
        this(context, null);
    }

    public CustomTabLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTabLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.item_tablayout_custom, this);
        tabLayoutCustom = findViewById(R.id.tabLayoutCustom);
    }

    public void setListAdapter(List<MenuBean> menuBeanList, ViewPager2 viewPager2) {
        mMenuBeanList = menuBeanList;
        if (tabLayoutCustom != null) {
            setViewPagerWithTab(tabLayoutCustom, viewPager2);
            tabLayoutCustom.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (tab.getCustomView() != null) {
                        final TextView tvCustomTabName = tab.getCustomView().findViewById(R.id.tvCustomTabName);
                        final View view = tab.getCustomView().findViewById(R.id.viewCustomLine);
                        if (tvCustomTabName != null) {
                            tvCustomTabName.setTextColor(getResources().getColor(R.color.black));
                        }
                        setTextViewColor(view);
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    if (tab.getCustomView() != null) {
                        final TextView tvCustomTabName = tab.getCustomView().findViewById(R.id.tvCustomTabName);
                        final View view = tab.getCustomView().findViewById(R.id.viewCustomLine);
                        if (tvCustomTabName != null) {
                            tvCustomTabName.setTextColor(getResources().getColor(R.color.gray1));
                        }
                        if (view != null) {
                            view.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }

    private void setTextViewColor(View view) {
        if (view != null) {
            if (ShoppingAppInitUtils.Companion.getInstance().getTHEME_COLOR() != 0) {
                view.setBackgroundColor(getResources().getColor(ShoppingAppInitUtils.Companion.getInstance().getTHEME_COLOR()));
            } else {
                view.setBackgroundColor(getResources().getColor(R.color.black));
            }
        }
    }

    /**
     * 设置 tabLayout 和viewPager结合
     * *
     * * @param tabLayout
     * * @param viewPager2
     */
    private void setViewPagerWithTab(TabLayout tabLayout, ViewPager2 viewPager2) { //用来对tabLayout 和 viewPager 进行结合
        final TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                if (mMenuBeanList != null) {
                    final String title = mMenuBeanList.get(position).getTitle();
                    if (!TextUtils.isEmpty(title)) {
                        tab.setText(title);
                    }
                }
            }
        });
        tabLayoutMediator.attach();
        //注意结合完以后再调用这个方法，就可以很好的获取tab的个数，能够更好的自定义
        initCustomTab(tabLayout);
    }

    /**
     * 自定义 tabLayout
     */
    private void initCustomTab(TabLayout tabLayout) {
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                final View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_tablayout, null);
                final TextView textView = view.findViewById(R.id.tvCustomTabName);
                final View lineView = view.findViewById(R.id.viewCustomLine);
                if (textView != null) {
                    if (i == 0) {
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        textView.setTextColor(getResources().getColor(R.color.gray1));
                    }
                    if (mMenuBeanList != null) {
                        if (!TextUtils.isEmpty(mMenuBeanList.get(i).getTitle())) {
                            textView.setText(mMenuBeanList.get(i).getTitle());
                        }
                    }
                }
                if (lineView != null) {
                    if (i == 0) {
                        setTextViewColor(lineView);
                    }
                }
                tab.setCustomView(view);

            }
        }
    }


}
