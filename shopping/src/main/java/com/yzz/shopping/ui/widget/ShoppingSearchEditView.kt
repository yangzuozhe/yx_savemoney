package com.yzz.shopping.ui.widget

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.yzz.shopping.R
import kotlinx.android.synthetic.main.item_custom_search.view.*

/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/3 13:57
 *
 * @Description : ShoppingSearchEditView
 */
class ShoppingSearchEditView : FrameLayout {
    private var mClearSearchListener: MyClickListener? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.item_custom_search, this)
        mIvClearSearch?.setOnClickListener {
            showSoftInput(mIvClearSearch)
            mEdShoppingShopping?.text?.clear()
            mClearSearchListener?.onSuccess()
        }
    }

    fun setOnClearSearchListener(clearSearchListener: MyClickListener) {
        mClearSearchListener = clearSearchListener
    }

    fun setOnLeftClick(l: MyClickListener?) {
        mIvShoppingBack?.setOnClickListener {
            l?.onSuccess()
        }
        mIvShoppingBack?.visibility = View.VISIBLE
    }

    fun setOnSearchListener(l: SearchListener?) {
        mEdShoppingShopping?.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    val text = mEdShoppingShopping?.text
                    hideSoftInput()
                    l?.onSuccess(text)
                }
            }
            true
        }
    }

    fun setOnRightClick(l: MyClickListener?) {
        mTvShoppingSearchYes?.setOnClickListener {
            hideSoftInput()
            l?.onSuccess()
        }
        mTvShoppingSearchYes?.visibility = View.VISIBLE
    }

    fun getEditTextContent() = mEdShoppingShopping?.text.toString()

    fun setEditTextClick(l: MyClickListener?) {
        mEdShoppingShopping?.setOnClickListener {
            showSoftInput(it)
            l?.onSuccess()
        }
    }

    fun setEditTextContent(string: String?) {
        try {
            if (string != null && string.isNotEmpty()) {
                mEdShoppingShopping?.setText(string)
                mEdShoppingShopping?.setSelection(string.length)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showSoftInput(view: View?) {
        if (view != null) {
            val activity = view.context as Activity?
            val imm =
                activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            //显示键盘
            imm?.showSoftInput(view, InputMethodManager.SHOW_FORCED)
        }
    }

    fun hideSoftInput() {
        if (mEdShoppingShopping != null) {
            val activity = mEdShoppingShopping.context as Activity?
            val imm =
                activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            //ps：这是强制隐藏键盘的代码，因此如果想要显示键盘用上面那句话，隐藏键盘用下面这句话
            imm?.hideSoftInputFromWindow(mEdShoppingShopping.windowToken, 0)
        }
    }


    fun interface SearchListener {
        fun onSuccess(string: Editable?)
    }

    fun interface MyClickListener {
        fun onSuccess()
    }

}