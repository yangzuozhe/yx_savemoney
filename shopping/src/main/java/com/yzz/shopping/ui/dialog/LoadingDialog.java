package com.yzz.shopping.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.yzz.shopping.R;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/25 14:54
 * @Description : MyDialog
 */
public class LoadingDialog extends Dialog {

    public LoadingDialog(@NonNull Context context) {
        //设置自定义dialog的类型
        super(context, R.style.MyDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loadding);
        //按空白处不能取消动画
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    @Override
    public void show() {
        super.show();
        //下面这段代码可以让对话框出现在底部
        Window window = getWindow();
        if (window != null) {
            WindowManager.LayoutParams params = window.getAttributes();
//            params.gravity = Gravity.BOTTOM;
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(params);
            params.dimAmount = 0.0f;
            window.setBackgroundDrawableResource(R.color.transparent);

        }
    }



}