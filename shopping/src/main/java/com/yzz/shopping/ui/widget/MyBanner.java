package com.yzz.shopping.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.recyclerview.widget.RecyclerView;

import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerAdapter;
import com.yzz.shopping.utils.LoggerUtils;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/18 11:52
 * @Description : MyBanner
 */
public class MyBanner<T, BA extends BannerAdapter<T, ? extends RecyclerView.ViewHolder>> extends Banner<T, BA> {
    public MyBanner(Context context) {
        this(context, null);
    }

    public MyBanner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyBanner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private float mDownY;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDownY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                float delY = Math.abs(event.getY() - mDownY);
                LoggerUtils.d("yyzzmove",delY+"");
                if (delY > 30){
                    return true;
                }
                break;
        }
        return false;
    }


}
