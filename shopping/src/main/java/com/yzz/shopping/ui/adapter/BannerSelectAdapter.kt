package com.yzz.shopping.ui.adapter

import android.annotation.SuppressLint
import android.text.Layout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yzz.shopping.R
import com.yzz.shopping.entity.SelectChangeBean

/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/6 15:39
 *
 * @Description : BannerSelectAdapter
 */
class BannerSelectAdapter(private val mList: ArrayList<SelectChangeBean>?) :
    RecyclerView.Adapter<BannerSelectAdapter.BannerSelectViewHolder>() {
    class BannerSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mIvSelect: View? = itemView.findViewById<View>(R.id.mIvSelect)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerSelectViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_banner_select, parent, false)
        return BannerSelectViewHolder(view)
    }

    override fun onBindViewHolder(holder: BannerSelectViewHolder, position: Int) {
        val selectChangeBean: SelectChangeBean? = mList?.get(position)
        if (selectChangeBean != null) {
            holder.mIvSelect?.isSelected = selectChangeBean.isSelect
        }
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    @SuppressLint("NotifyDataSetChanged")
    fun changeSelect(position: Int) {
        if (position >= 0 && mList != null && position < mList.size) {
            mList.forEachIndexed { index, selectChangeBean ->
                selectChangeBean.isSelect = index == position
            }
            notifyDataSetChanged()
        }
    }
}