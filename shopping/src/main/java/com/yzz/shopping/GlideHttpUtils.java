package com.yzz.shopping;

import android.text.TextUtils;

import com.yzz.shopping.entity.BaseInfo;
import com.yzz.shopping.entity.GlideRecommendBean;
import com.yzz.shopping.entity.ShoppingHotBean;
import com.yzz.shopping.utils.ShoppingAppInitUtils;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.ObservableTransformer;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/22 9:35
 * @Description : HttpUtils
 */
public class GlideHttpUtils {
    private GlideHttpUtils() {

    }

    private static class SingltonHttpGlideUtils {
        private static final GlideHttpUtils INSTANCE = new GlideHttpUtils();
    }

    public static GlideHttpUtils getInstacne() {
        return SingltonHttpGlideUtils.INSTANCE;
    }

    private <T> ObservableTransformer<T, T> setThread() {
        return upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * @param ec        1-淘宝，2-京东，3-拼多多，4-聚合
     * @param pageSize  每页数量，默认20
     * @param pageIndex 页数
     */
    public void requestHomeRecommend(int ec, int pageSize, int pageIndex, GlideListener<ArrayList<GlideRecommendBean>> listener) {
        final HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("ec", ec);
        hashMap.put("pageSize", pageSize);
        hashMap.put("pageIndex", pageIndex);
        RxGlideService.createApi(GlideApiFunction.class)
                .requestHomeRecommend(GlideEncodeBeanUtils.getParam(hashMap))
                .subscribe(new Observer<BaseInfo<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BaseInfo<String> baseInfo) {
                        beanSuccess(baseInfo, listener);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.onError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * @param ec        1-淘宝，2-京东，3-拼多多，4-聚合
     * @param pageSize  每页数量，默认20
     * @param pageIndex 页数
     */
    public void requestRecommend(int ec, int pageSize, int pageIndex, GlideListener<ArrayList<GlideRecommendBean>> listener) {
        final HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("ec", ec);
        hashMap.put("pageSize", pageSize);
        hashMap.put("pageIndex", pageIndex);
        hashMap.put("types", String.valueOf(ShoppingAppInitUtils.Companion.getInstance().getWEATHER_TYPE()));
        RxGlideService.createApi(GlideApiFunction.class)
                .requestRecommend(GlideEncodeBeanUtils.getParam(hashMap))
                .subscribe(new Observer<BaseInfo<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BaseInfo<String> baseInfo) {
                        beanSuccess(baseInfo, listener);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.onError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * @param ec        1-淘宝，2-京东，3-拼多多，4-聚合
     * @param keyword   搜索关键词
     * @param pageSize  每页数量，默认20
     * @param pageIndex 页数
     * @param isCoupon  有无优惠券，0表示全部，1表示只看优惠券商品
     * @param sortName  0综合，1佣金比例降序，2销量降序，3价格降序，4价格升序
     */
    public void requestSearch(int ec, String keyword, int pageSize, int pageIndex, int isCoupon, int sortName, GlideListener<ArrayList<GlideRecommendBean>> listener) {
        final HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("ec", ec);
        hashMap.put("keyword", keyword);
        hashMap.put("pageSize", pageSize);
        hashMap.put("pageIndex", pageIndex);
        hashMap.put("isCoupon", isCoupon);
        hashMap.put("sortName", sortName);
        RxGlideService.createApi(GlideApiFunction.class)
                .requestSearch(GlideEncodeBeanUtils.getParam(hashMap))
                .compose(setThread())
                .subscribe(new Observer<BaseInfo<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BaseInfo<String> baseInfo) {
                        beanSuccess(baseInfo, listener);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.onError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void beanSuccess(@NonNull BaseInfo<String> baseInfo, GlideListener<ArrayList<GlideRecommendBean>> listener) {
        if (baseInfo.getStatus() == 200) {
            final String data = baseInfo.getData();
            if (!TextUtils.isEmpty(data)) {
                final ArrayList<GlideRecommendBean> list = GlideEncodeBeanUtils.encodeCityBeanListNew(data, GlideRecommendBean.class, true);
                if (list != null && !list.isEmpty()) {
                    listener.onSuccess(list);
                } else {
                    listener.onError("list Empty");
                }
            } else {
                listener.onError("data Empty");
            }
        } else {
            listener.onError("not 200");
        }
    }

    /**
     * 淘宝无需请求此接口，直接打开couponUrl
     *
     * @param materialUrl 搜索返回的商品详情地址
     * @param couponUrl   搜索返回的商品优惠券地址，无则传空
     * @param ec          搜索时返回2-京东，3-拼多多
     */
    public void requestJumUrl(String materialUrl, String couponUrl, String ec, GlideListener<String> listener) {
        final HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("materialUrl", materialUrl);
        hashMap.put("couponUrl", couponUrl);
        hashMap.put("ec", ec);
        RxGlideService.createApi(GlideApiFunction.class)
                .requestJumUrl(GlideEncodeBeanUtils.getParam(hashMap))
                .compose(setThread())
                .subscribe(new Observer<BaseInfo<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BaseInfo<String> baseInfo) {
                        if (baseInfo.getStatus() == 200) {
                            final String data = baseInfo.getData();
                            listener.onSuccess(data);
                        } else {
                            listener.onError("not 200");
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.onError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void postAdminUpGoods(String userToken, GlideRecommendBean bean, GlideListener<String> listener) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("ec", bean.getEc());
        hashMap.put("ecName", bean.getEcName());
        hashMap.put("discount", bean.getDiscount());
        hashMap.put("imageUrl", bean.getImageUrl());
        hashMap.put("itemId", bean.getItemId());
        hashMap.put("skuName", bean.getSkuName());
        hashMap.put("shopName", bean.getShopName());
        hashMap.put("lowestCouponPrice", bean.getLowestCouponPrice());
        hashMap.put("commission", bean.getCommission());
        hashMap.put("commissionShare", bean.getCommissionShare());
        hashMap.put("inOrderCount30Days", bean.getInOrderCount30Days());
        hashMap.put("materialUrl", bean.getMaterialUrl());
        hashMap.put("couponUrl", bean.getCouponUrl());
        hashMap.put("price", bean.getPrice());
        HashMap<String, Object> hashMap1 = GlideEncodeBeanUtils.getParam(hashMap);
        hashMap1.put("to", userToken);
        RxGlideService.createApi(GlideApiFunction.class)
                .postAdminUpGoods(hashMap1)
                .compose(setThread())
                .subscribe(new Observer<BaseInfo>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BaseInfo baseInfo) {
                        if (baseInfo.getStatus() == 200) {
                            listener.onSuccess("成功");
                        } else {
                            listener.onError("");
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.onError("");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void requestHost(int pageSize, GlideListener<ShoppingHotBean> listener) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("pageSize", pageSize);
//        hashMap.put("pageIndex", pageIndex);
        RxGlideService.createApi(GlideApiFunction.class)
                .requestHotRecommend(GlideEncodeBeanUtils.getParam(hashMap))
                .compose(setThread())
                .subscribe(new Observer<BaseInfo<String>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BaseInfo<String> baseInfo) {
                        if (baseInfo.getStatus() == 200) {
                            ShoppingHotBean shoppingHotBean = GlideEncodeBeanUtils.encodeCityBean(baseInfo.getData(), ShoppingHotBean.class);
                            if (shoppingHotBean != null) {
                                listener.onSuccess(shoppingHotBean);
                            } else {
                                listener.onError("list is empty");
                            }
                        } else {
                            listener.onError("state not 200");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.onError("error");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public interface GlideListener<T> {
        void onSuccess(T bean);

        void onError(String e);
    }

}
