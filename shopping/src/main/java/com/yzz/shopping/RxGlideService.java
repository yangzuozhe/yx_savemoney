package com.yzz.shopping;


import com.yzz.shopping.utils.ShoppingAppInitUtils;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/22 9:34
 * @Description : RxService
 */
public class RxGlideService {
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ShoppingAppInitUtils.Companion.getInstance().getGLIDE_HTTP_URL())
            //.client(ClientFactory.INSTANCE.getHttpClient())  //不打印 网路请求
            .client(ClientFactory.INSTANCE.getOkHttpClient())//打印 网路请求
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(new NullOnEmptyConverterFactory()) // 解决返回数据为空错误
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private RxGlideService() {
        //construct
    }

    public static <T> T createApi(Class<T> clazz) {
        return retrofit.create(clazz);
    }


}