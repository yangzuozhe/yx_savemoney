package com.yzz.shopping;


import com.yzz.shopping.entity.BaseInfo;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/22 9:39
 * @Description : GlideApiFunction
 */
public interface GlideApiFunction {
    @POST("guide/api/recommend/")
    @FormUrlEncoded
    Observable<BaseInfo<String>> requestRecommend(@FieldMap HashMap<String, Object> map);

    @POST("guide/api/home/")
    @FormUrlEncoded
    Observable<BaseInfo<String>> requestHomeRecommend(@FieldMap HashMap<String, Object> map);

    @POST("guide/api/so/")
    @FormUrlEncoded
    Observable<BaseInfo<String>> requestSearch(@FieldMap HashMap<String, Object> map);

    @POST("guide/api/promotion/")
    @FormUrlEncoded
    Observable<BaseInfo<String>> requestJumUrl(@FieldMap HashMap<String, Object> map );


    @POST("rebate/api/adminUpGoods/")
    @FormUrlEncoded
    Observable<BaseInfo> postAdminUpGoods(@FieldMap HashMap<String,Object> hashMap);

    @POST("guide/api/hot2/")
    @FormUrlEncoded
    Observable<BaseInfo<String>> requestHotRecommend(@FieldMap HashMap<String,Object> hashMap);
}
