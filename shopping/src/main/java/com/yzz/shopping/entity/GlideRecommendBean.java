package com.yzz.shopping.entity;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/22 9:41
 * @Description : GlideRecommendBean
 */
public class GlideRecommendBean {
    private String ec;
    private String ecName;
    private String discount;
    private String imageUrl;
    private String skuName;
    private String shopName;
    private String lowestCouponPrice;
    private String price;
    private String commission;
    private String commissionShare;
    private String inOrderCount30Days;
    private String materialUrl;
    private String couponUrl;
    private String itemId;
    /**
     * hid : 569
     */

    private int hid;

    public String getEc() {
        return ec;
    }

    public void setEc(String ec) {
        this.ec = ec;
    }

    public String getEcName() {
        return ecName;
    }

    public void setEcName(String ecName) {
        this.ecName = ecName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getLowestCouponPrice() {
        return lowestCouponPrice;
    }

    public void setLowestCouponPrice(String lowestCouponPrice) {
        this.lowestCouponPrice = lowestCouponPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCommissionShare() {
        return commissionShare;
    }

    public void setCommissionShare(String commissionShare) {
        this.commissionShare = commissionShare;
    }

    public String getInOrderCount30Days() {
        return inOrderCount30Days;
    }

    public void setInOrderCount30Days(String inOrderCount30Days) {
        this.inOrderCount30Days = inOrderCount30Days;
    }

    public String getMaterialUrl() {
        return materialUrl;
    }

    public void setMaterialUrl(String materialUrl) {
        this.materialUrl = materialUrl;
    }

    public String getCouponUrl() {
        return couponUrl;
    }

    public void setCouponUrl(String couponUrl) {
        this.couponUrl = couponUrl;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }


    public int getHid() {
        return hid;
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    @Override
    public String toString() {
        return "GlideRecommendBean{" +
                "ec='" + ec + '\'' +
                ", ecName='" + ecName + '\'' +
                ", discount='" + discount + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", skuName='" + skuName + '\'' +
                ", shopName='" + shopName + '\'' +
                ", lowestCouponPrice='" + lowestCouponPrice + '\'' +
                ", price='" + price + '\'' +
                ", commission='" + commission + '\'' +
                ", commissionShare='" + commissionShare + '\'' +
                ", inOrderCount30Days='" + inOrderCount30Days + '\'' +
                ", materialUrl='" + materialUrl + '\'' +
                ", couponUrl='" + couponUrl + '\'' +
                ", itemId='" + itemId + '\'' +
                '}';
    }
}
