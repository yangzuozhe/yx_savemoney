package com.yzz.shopping.entity

/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/6 16:13
 *
 * @Description : SelectChangeBean
 */
data class SelectChangeBean(var isSelect: Boolean = false)
