package com.yzz.shopping.entity;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/2/24 14:56
 * @Description : BaseInfo
 */
public class BaseInfo<T> {
    private int status;
    private int code;
    private String msg;
    private T data;
    private T result;
    private String message;
    private String ResultCode;
    private String ResultMessage;
    private T Data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getResultCode() {
        return ResultCode;
    }

    public void setResultCode(String resultCode) {
        ResultCode = resultCode;
    }

    public String getResultMessage() {
        return ResultMessage;
    }

    public void setResultMessage(String resultMessage) {
        ResultMessage = resultMessage;
    }

    public boolean isSuccess(){
        int SUCCESS_CODE = 0;
        return getStatus()== SUCCESS_CODE;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setmData(T data) {
        this.data = data;
    }

    public T getmData() {
        return Data;
    }

    public void setData(T data) {
        this.Data = data;
    }
}
