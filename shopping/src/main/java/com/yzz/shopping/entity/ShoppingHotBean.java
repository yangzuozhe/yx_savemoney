package com.yzz.shopping.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @Author : yangzuozhe
 * @Time : On 2022/5/9 9:52
 * @Description : ShoppingHotBean
 */
public class ShoppingHotBean {

    /**
     * title : 大家都在买
     * gotoText : 直达聚划算
     * gotoUrl : magicvirtual://dl/intent/tbopen://m.taobao.com/tbopen/index.html?h5Url=https%3A%2F%2Fs.click.taobao.com%2Ft%3Funion_lens%3DlensId%3AOPT%401650854359%402108254e_08b7_1805e967242_b003%4001%3BeventPageId%3A20150318020000462%26e%3Dm%3D2%26s%3DucBeSKOqe%2FBw4vFB6t2Z2iperVdZeJviPI5Rhak06vZnX1vWUft3ZQTBTL2ADUK2uu1yIUYt4r7V5mvodwms1%2F%2BMbUNkb8GXz7Dq7qEbmzMXj7%2FaV8SopEbh3CPs%2BWfVqwgOWsQKa%2FiBN3iHUGXXBt0zE%2BcOPMULdlHg3xH9IGq0GzdAMqThcEZtDKPoHWr7QBRB1XJCO2%2FV0qUZWiWhJWiXtl3eKZAZcnjQKEJbkmoyv5qZyLTEdFWiQTNxc%2BvoD07VP3SBL1mUcH407eK450pCiJnpS3o9lAe0T%2FAhb0ZTG%2FZ3Je4HhfD8DpA%2BLpWXaWI7IuLLgqvYnFmTicdn3nh5%2BvPkJ2DxDcbShJUCG%2BqGO6u9yUjXh5cPBBwuFAPAxiXvDf8DaRs%3D
     */

    private TitleBarBean titleBar;
    /**
     * hid : 569
     * ec : 1
     * ecName : 淘宝
     * discount : 11.0
     * imageUrl : https://img.alicdn.com/i2/444903057/O1CN01QtHgwJ1YS9WfNyv7E_!!0-item_pic.jpg
     * itemId : 595236456917
     * skuName : 邦怡洗脸巾女一次性棉柔抽取式擦脸巾纯棉洁面巾纸洗面巾干湿两用
     * shopName : 邦怡旗舰店
     * lowestCouponPrice : 6.9
     * price : 17.9
     * commission : 2.76
     * commissionShare : 40.0
     * inOrderCount30Days : 200
     * materialUrl : http://item.taobao.com/item.htm?id=595236456917
     * couponUrl : https://uland.taobao.com/coupon/edetail?e=6xIps58V6qENfLV8niU3R5TgU2jJNKOfNNtsjZw%2F%2FoK1VYo%2FLOpr3h0J5z76387cnVaj9E5sdulvyG5l61t7ubhtWWjRAlP0awvDoeqYhH3i1Zb%2BDXN36v7cdpVC2RDlDfqEFBOhTczDFpoHkJ%2FJ%2FVgYJrMihnsL0%2FlDl8aOvkObtiJW52nYoeW4BgFGCWxt&&app_pvid=59590_33.5.101.205_692_1650852044988&ptl=floorId:31507;app_pvid:59590_33.5.101.205_692_1650852044988;tpp_pvid:&union_lens=lensId%3AOPT%401650852045%40210565cd_081e_1805e73219c_4589%4001
     */
    @SerializedName("goodsList")
    private ArrayList<GlideRecommendBean> glideRecommendBeanArrayList;

    public TitleBarBean getTitleBar() {
        return titleBar;
    }

    public void setTitleBar(TitleBarBean titleBar) {
        this.titleBar = titleBar;
    }


    public ArrayList<GlideRecommendBean> getGlideRecommendBeanArrayList() {
        return glideRecommendBeanArrayList;
    }

    public void setGlideRecommendBeanArrayList(ArrayList<GlideRecommendBean> glideRecommendBeanArrayList) {
        this.glideRecommendBeanArrayList = glideRecommendBeanArrayList;
    }

    public static class TitleBarBean {
        private String title;
        private String gotoText;
        private String gotoUrl;
        private String gotoIcon;
        private String titleIcon;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getGotoText() {
            return gotoText;
        }

        public void setGotoText(String gotoText) {
            this.gotoText = gotoText;
        }

        public String getGotoUrl() {
            return gotoUrl;
        }

        public void setGotoUrl(String gotoUrl) {
            this.gotoUrl = gotoUrl;
        }

        public String getGotoIcon() {
            return gotoIcon;
        }

        public void setGotoIcon(String gotoIcon) {
            this.gotoIcon = gotoIcon;
        }

        public String getTitleIcon() {
            return titleIcon;
        }

        public void setTitleIcon(String titleIcon) {
            this.titleIcon = titleIcon;
        }
    }

}
