package com.yzz.shopping.entity;

import java.io.Serializable;

public class MenuBean implements Serializable {

    /**
     * title : 祝福
     * type : 1
     */

    private String title;
    private String type;

    public MenuBean(String title, String type) {
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
