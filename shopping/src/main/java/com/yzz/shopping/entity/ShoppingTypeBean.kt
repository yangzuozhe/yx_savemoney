package com.yzz.shopping.entity

/**
 * @Author : yangzuozhe
 * @Time : On 2022/3/4 11:32
 *
 * @Description : ShoppingTypeBeab
 */
data class ShoppingTypeBean(
    val type: Int,
    val typeName: String,
    val typeUpImg: Int = 0,
    val typeUnSelectImg: Int = 0,
    val typeImgRotation: Int = 0,
    var mIsSelect: Boolean = false
)