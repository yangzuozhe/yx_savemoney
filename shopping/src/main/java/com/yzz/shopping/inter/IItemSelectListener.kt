package com.yzz.shopping.inter

/**
 * @Author : yangzuozhe
 * @Time : On 2022/4/20 9:25
 *
 * @Description : IUmengSelectListener
 */
interface IItemSelectListener<T> {
    fun onClick(bean: T)
}