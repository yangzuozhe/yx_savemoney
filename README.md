# yx_shopping

# 使用方法

## 需要在 app 模块的 AndroidManifest.xml 中添加如下代码:
```xml
        <activity
            android:name="com.kepler.jd.sdk.KeplerBackActivity"
            android:exported="true"
            android:theme="@android:style/Theme.Translucent">
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />
                <category android:name="android.intent.category.DEFAULT" />
                <data android:scheme="写入京东唤起app的appkey" />
            </intent-filter>
        </activity>
```
如果有如下权限要添加语句：
```xml
  <!--   要记得淘宝的sdk 需要在以下这些权限加上 tools:node="merge" 否则，无法使用 -->
    <uses-permission
        android:name="android.permission.WRITE_EXTERNAL_STORAGE"
        tools:node="merge" />
    <uses-permission
        android:name="android.permission.READ_EXTERNAL_STORAGE"
        tools:node="merge" />
    <uses-permission
        android:name="android.permission.BLUETOOTH"
        tools:node="merge" />
    <uses-permission
        android:name="android.permission.GET_TASKS"
        tools:node="merge" />
```

## 2.在在线参数写如下
```java
            final String shoppingTitle = OnlineConfigHelper.findConfigValue(ShoppingPreferenceUtil.SHOP_TITLE, data);
            ShoppingPreferenceUtil.setTitleParam(this, shoppingTitle);

        ShoppingPreferenceUtil.setWhiteListParam(this, OnlineConfigHelper.findConfigValue(ShoppingPreferenceUtil.WHITE_LIST,data));
```
## 3.安全文件(safe.jpg)放在 app 模块里的 res/raw/safe.jpg  当中
## 3.加入阿里百川安全文件（yw_1222_baichuan.jpg）到 res/drawable
## 4.将淘宝aar文件放到 app 模块的libs中
## 5.ShoppingAppInitUtils 方法在 application 中初始化
## 6.添加淘宝混淆
```
#阿里百川
-keepattributes Signature
-ignorewarnings
-keep class javax.ws.rs.** { *; }
-keep class com.alibaba.fastjson.** { *; }
-dontwarn com.alibaba.fastjson.**
-keep class sun.misc.Unsafe { *; }
-dontwarn sun.misc.**
-keep class com.taobao.** {*;}
-keep class com.alibaba.** {*;}
-dontwarn com.taobao.**
-dontwarn com.alibaba.**
-keep class com.ta.** {*;}
-dontwarn com.ta.**
-keep class org.json.** {*;}

-keep class tv.danmaku.ijk.media.player.TaobaoMediaPlayer{*;}
-keep class tv.danmaku.ijk.media.player.TaobaoMediaPlayer$*{*;}
```
## 7.根目录build.gradle 的 repositories 添加如下代码：
这是轮播图需要的
```gradle
        maven { url "https://s01.oss.sonatype.org/content/groups/public" }
```
## 8.轮播图调用：
在xml 布局中写入 BannerView 
在代码中进行调用,最后一个参数是请求的轮播图总页数
activity 中
```kotlin
mBannerView.requestActivityHot(activity,totalNumber,looptime)
```
或者fragment 中
```kotlin
mBannerView.requestFragmentHot(activity,fragment,totalNumber,looptime)
```